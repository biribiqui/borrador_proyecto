const io = require("../io");
const crypt = require("../crypt");

const requestJson = require("request-json");

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechuagm12ed/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1(req, res){

    console.log("GET /apitech/users");

    var users = require("../usuarios.json");
    var salida = {};

    if (req.query.$top != "")
    {
      var ocurrencias = req.query.$top;
    }
    else
    {
      var ocurrencias = users.length
    }

    salida.usuarios = users.slice(0, ocurrencias);

    if (req.query.$count == "true")
    {
      salida.count = users.length;
    }

    res.send(salida);
}

function createUserV1(req, res){

  console.log("POST /apitechu/v1/users");
  console.log(req);

  console.log(req.body);
  console.log(req.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.e_mail);

  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.e_mail
    }

  var users = require("../usuarios.json");

  users.push(newUser);

  io.writeUserDataToFile(users)
}

function deleteUserV1(req, res){
  console.log("DELETE /apitechu/v1/users/:id");

  console.log("La ID del usuario a borrar es " + req.params.id);

  var users = require("../usuarios.json");

  var encontrado = false;

//       INICIO for
//  for (var i = 0; i < users.length; i++)
//  {
//    if (users[i].id == req.params.id)
//    {
//      encontrado = true;

//      break;
//    }
//  }
//       FIN for

//       INICIO for of
//  var i = 0;

//  for (user of users)
//  {
//    if (user.id == req.params.id)
//    {
//      encontrado = true;

//      break;
//    }
//    else
//    {
//      i++;
//    }
//  }
//     FIN for of

//     INICIO for in
//  for (var i = 0; i < users.length; i++)
//  {
//    for (n in users[i])
//    {
//      if (users[i][n] == req.params.id && n == "id")
//      {
//        encontrado = true;

//        break;
//      }
//    }

//    if (encontrado)
//    {
//      break;
//    }
//    else
//    {
//      ++i
//    }
//  }
//      FIN for in

//     INICIO forEach
//  var i;

//  users.forEach(function(item, index)
//  {
//    if (item.id == req.params.id)
//    {
//      encontrado = true;

//      i = index;
//    }
//  })
//       FIN forEach

//      INICIO findIndex
var i = users.findIndex(function(element)
{
  return element.id == req.params.id;
})

if (i != -1)
{
  encontrado = true;
}
//      FIN findIndex

  if (encontrado)
  {
    console.log("borrando elemento " + i);

    users.splice(i, 1);

    io.writeUserDataToFile(users);
  }
  else
  {
    console.log("elemento no encontrado");
  }

  res.send(users);
}

function getUsersV2(req, res){

    console.log("GET /apitechu/v2/users");

    var httpClient = requestJson.createClient(baseMLABUrl);

    console.log("Cliente creado");

    httpClient.get("user?" + mlabAPIKey, function(err, resMlab, body){
      var response = !err ? body : {
        "msg": "Error obteniendo usuarios"
      }

      res.send(response);
    }
  );
}

function getUserByIdV2(req, res){

    console.log("GET /apitechu/v2/users/:id");

    var httpClient = requestJson.createClient(baseMLABUrl);

    console.log("Cliente creado");

    var id = Number.parseInt(req.params.id);

    var query = "q=" + JSON.stringify({"id": id});

    console.log("La ID del usuario a obtener es: " + id);
    console.log("datos API " +"user?" + query + "&" + mlabAPIKey);

    httpClient.get("user?" + query + "&" + mlabAPIKey, function(err, resMlab, body){
      if (err){
        var response = {"msg": "Error obteniendo usuarios"};
      }
      else {
        if (body.length > 0) {
          var response = body[0];
        }
        else {
          var response = {"msg": "Usuario no encontrado"};
          res.status(404);
        }
      }

      res.send(response);
    }
  );
}

function createUserV2(req, res){

    console.log("POST /apitechu/v2/users/");

    var httpClient = requestJson.createClient(baseMLABUrl);

    console.log(req);

    console.log(req.body);
    console.log(req.id);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.e_mail);

    var newUser = {
      "id":req.body.id,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.e_mail,
      "password":crypt.hash(req.body.password)
    }

    httpClient.post("user?" + mlabAPIKey, newUser,
      function(err, resMlab, body){
        console.log("Usuario creado");
        res.status(201).send({"msg":"Usuario Creado"});
      })
}

function createUserV3(req, res){

  console.log("POST /apitechu/v3/users");

  var query = 'q={"$and":[{"activo":true},' + JSON.stringify({"email":req.body.email}) + ']}';

  var httpClient = requestJson.createClient(baseMLABUrl);

  httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body){
      if (err){
         var response = {"msg": "Error en alta usuario"};

         res.send(response);
       }
      else {
        if (body.length > 0){

          var response = {"msg": "Usuario ya existe"};

          res.send(response);
        }
        else {
         query = 'f={"id":1}&l=1&s={"id":-1}';

         httpClient.get("user?" + query + "&" + mlabAPIKey,
          function(err, resMlab, getidbody){
            if (err){
                var response = {"msg": "Error en alta usuario"};

                res.send(response);
            }
            else {
              if (getidbody.length == 0){
                  var idUsuario = 1;
              }
              else {
                  var idUsuario = Number.parseInt(getidbody[0].id) + 1;
              }

              var newUser = {
                "id":idUsuario,
                "first_name":req.body.nombre,
                "last_name":req.body.apellido,
                "domicilio":req.body.domicilio,
                "provincia":req.body.provincia,
                "codposdtal":req.body.codpostal,
                "nif":req.body.nif,
                "telefono":req.body.telefono,
                "email":req.body.email,
                "password":crypt.hash(req.body.password),
                "activo":true,
                "fecalta": req.body.fecalta,
                "logged":true
              }

              httpClient.post("user?" + mlabAPIKey, newUser,
                function(err, resMlab, postbody){
                  if (err){

                    var response = {"msg": "Error en alta usuario"};

                    res.send(response);
                  }
                  else {
                    console.log("Usuario creado");

                    res.send({"msg":"Usuario Creado", "id":idUsuario});
                    }
                  }
                );
              }
            }
          );
        }
      }
    }
  );
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV3 = createUserV3;
