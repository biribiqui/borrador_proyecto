const requestJson = require("request-json");

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechuagm12ed/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getAccountsById(req, res){

    console.log("GET /apitechu/v2/accounts/:id");

    var httpClient = requestJson.createClient(baseMLABUrl);

    console.log("Cliente creado");

    var id = Number.parseInt(req.params.id);

    var query = "q=" + JSON.stringify({"idusuario": id});

    console.log("La ID de usuario de la cuenta a obtener es: " + id);

    httpClient.get("account?" + query + "&" + mlabAPIKey, function(err, resMlab, body){
      if (err){
        var response = {"msg": "Error obteniendo cuentas"};
      }
      else {
        if (body.length > 0) {
          var response = body;
        }
        else {
          var response = {"msg": "Usuario sin cuentas"};
          res.status(404);
        }
      }

      res.send(response);
    }
  );
}

module.exports.getAccountsById = getAccountsById;
