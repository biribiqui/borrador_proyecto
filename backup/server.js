require("dotenv").config();

const express = require("express");
const app = express();

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

app.use(express.json());
app.use(enableCORS);

const userController = require("./controllers/userController");
const authController = require("./controllers/authController");
const accountController = require("./controllers/accountController");

const port = process.env.PORT || 3000;

app.listen(port);

console.log("API escuchando en el puerto tururu " + port);

app.get("/apitechu/v1/hello",
  function(req, res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg":"Hola desde API Techu"});
  }

)

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("parametros");
    console.log(req.params);

    console.log("query string");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("body");
    console.log(req.body);

//    res.send("respuesta")

  }
)

//app.get("/apitechu/v1/users",
//  function(req, res){
//    console.log("GET /apitechu/v1/users");

//    res.sendFile("usuarios.json", {root: __dirname});

//    var users = require("./usuarios.json");

//    res.send(users);
//  }

//)

app.post("/apitechu/v1/users", userController.createUserV1);

app.post("/apitechu/v2/users", userController.createUserV2);

app.get("/apitechu/v1/users", userController.getUsersV1);

app.get("/apitechu/v2/users", userController.getUsersV2);

app.get("/apitechu/v2/users/:id", userController.getUserByIdV2);

app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);

app.post("/apitechu/v1/login/", authController.logonUserV1);

app.post("/apitechu/v2/login/", authController.logonUserV2);

app.post("/apitechu/v1/logout/:id", authController.logoutUserV1);

app.post("/apitechu/v2/logout/:id", authController.logoutUserV2);

app.get("/apitechu/v2/accounts/:id", accountController.getAccountsById);

app.post("/apitechu/v3/users", userController.createUserV3);
