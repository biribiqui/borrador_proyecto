const requestJson = require("request-json");

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechuagm12ed/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getAccountsById(req, res){

    console.log("GET /apitechu/accounts/:id");

    var httpClient = requestJson.createClient(baseMLABUrl);

    console.log("Cliente creado");

    var id = Number.parseInt(req.params.id);

    var query = "q=" + JSON.stringify({"idusuario": id}) + '&s={"fecalta":-1}';

    console.log("La ID de usuario de la cuenta a obtener es: " + id);

    httpClient.get("account?" + query + "&" + mlabAPIKey, function(err, resMlab, body){
      if (err){
        var response = {"msg": "Error obteniendo cuentas"};
      }
      else {
        if (body.length > 0) {
          var response = body;
        }
        else {
          var response = {"msg": "Usuario sin cuentas"};
//          res.status(404);
        }
      }

      res.send(response);
    }
  );
}

function getAccountsByIban(req, res){

    console.log("GET /apitechu/accounts/iban");

    var httpClient = requestJson.createClient(baseMLABUrl);

    console.log("Cliente creado");

    var query = 'q={"$and":[' + JSON.stringify({"iban": req.body.iban}) + ',' + '{"estado":"ACTIVA"}]}';
    console.log("El IBAN de la cuenta a obtener es: " + query);

    httpClient.get("account?" + query + "&" + mlabAPIKey, function(err, resMlab, body){
      if (err){
        var response = {"msg": "Error obteniendo cuenta"};
      }
      else {
        if (body.length > 0) {

          var response = body;
        }
        else {
          var response = {"msg": "Folio no existe"};
        }
      }

      res.send(response);
    }
  );
}

function createAccount(req, res){

  console.log("GET /apitechu/accounts/alta");

  var httpClient = requestJson.createClient(baseMLABUrl);

  console.log("Cliente creado");

  var query = 'f={"idfolio":1}&l=1&s={"idfolio":-1}';

  httpClient.get("account?" + query + "&" + mlabAPIKey,
   function(err, resMlab, getbody){
     if (err){
         var response = {"msg": "Error en alta de cuenta"};

         res.send(response);
     }
     else {
       if (getbody.length == 0){

           var idFolio = 1;
       }
       else {
           var idFolio = Number.parseInt(getbody[0].idfolio) + 1;
       }

       var folioString = idFolio.toString();

       for (var i = folioString.length; i < 8; i++){

         folioString = "0" + folioString;
       }

       var idCuenta= req.body.banco + req.body.oficina + req.body.contr + folioString;

       var d = new Date();
       var n = d.getTime();
       var nString = n.toString();

       var i = nString.length - 2;

       var nString2 = nString[i] + nString[i+1];

       var iban1 = 'ES' + nString2;
       var iban2 = req.body.banco;
       var iban3 = req.body.oficina;
       var iban4 = req.body.contr;
       var iban5 = folioString.substring(0, 4);
       var iban6 = folioString.substring(4, 8);

       var newAccount = {
         "idusuario":req.body.idUsuario,
         "iban":iban1 + " " + iban2 + " " + iban3 + " " + iban4 + " " + iban5 + " " + iban6,
         "idfolio":idFolio,
         "idcuenta": idCuenta,
         "banco":req.body.banco,
         "oficina":req.body.oficina,
         "contr":req.body.contr,
         "producto":req.body.producto,
         "saldo":req.body.saldo,
         "limite":req.body.limite,
         "intervencion":req.body.intervencion,
         "estado":"ACTIVA",
         "bloqcargo":req.body.blqcargo,
         "bloqabono":req.body.blqabono,
         "fecalta":req.body.fecalta,
         "notas":req.body.notas
       }

       httpClient.post("account?" + mlabAPIKey, newAccount,
         function(err, resMlab, postbody){
           if (err){

             var response = {"msg": "Error en alta de cuenta"};

             res.send(response);
           }
           else {
             console.log("Cuenta creada");

             res.send({"msg":"Cuenta Creada", "idUsuario":req.body.idUsuario, "idfolio": idFolio});
           }
         }
       );
     }
    }
  );
}

function updateAccount(req, res){
  console.log("POST /apitechu/accounts/update/:id ");

  console.log("El id del usuario a modificar " + req.params.id + " cuenta " + req.body.idfolio + " limite " + req.body.limite);

  var id = Number.parseInt(req.params.id);
  var idfolio = Number.parseInt(req.body.idfolio);

  var query = 'q={"$and":[' + JSON.stringify({"idusuario": id}) + ',' + JSON.stringify({"idfolio": idfolio}) + ']}';

  var httpClient = requestJson.createClient(baseMLABUrl);

  httpClient.get("account?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body){

      if (err)
      {
        var response = {"msg": "Error en modificación de cuenta"};

        res.send(response);
      }
      else
      {
        if (body.length > 0)
        {
            if (req.body.operacion == "saldo"){

            var putBody = '{"$set":{' +
              '"saldo":' + req.body.saldo +
              '}}';
          }else {

            if (req.body.operacion == "limite" ){
              var putBody = '{"$set":{' +
                '"limite":' + req.body.limite +
                '}}';
            }else {
              var putBody = '{"$set":{' +
                '"estado":"' + req.body.estado + '", ' +
                '"fecancel":"' + req.body.fecancel + '"' +
                '}}';
            }
          }

          httpClient.put("account?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
            function(err, resMlab, bodyPut){
              if (err)
              {
                var response = {"msg": "Error en modificación de cuenta"};

                res.send(response);
              }
              else
              {
                var response = {"msg":"Modificación correcta", "idusuario":req.param.id, "idfolio":req.body.idfolio};

                res.send(response);
              }
            }
          );
        }
        else
        {
          var response = {"msg": "Cuenta no encontrada"};
          res.status(404);

          res.send(response);
        }
      }
    }
  );
}

module.exports.getAccountsById = getAccountsById;
module.exports.getAccountsByIban = getAccountsByIban;
module.exports.createAccount = createAccount;
module.exports.updateAccount = updateAccount;
