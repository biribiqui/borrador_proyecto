const requestJson = require("request-json");

const loansAPIKey = process.env.loansAPIKey;

function calculoCuadro(req, res){

    console.log("GET /apitechu/loans/cuadro/:apiKey");

    if (req.params.apiKey == loansAPIKey){

      response = calcularCuadro(req.body);
    }else {

      response = {"msg": "Error, APIKEY incorrecta"};
    }

    res.send(response);
}

function calcularCuadro(datos) {

  var cuadroRespuesta = [];
  var respuesta = {};

  var Ptmo = datos;

  var cuadroAmort = CuadroAmortizacion(Ptmo).Cuadro;

  vtosAnno = calcular_vtosanno(Ptmo.perAmo);

  var porcTae = calcularTAE(Ptmo.importeInicial, cuadroAmort, Ptmo.fechaInicio, Ptmo.diaPago, 0 , 0, 0, 0, 0, 0, 0, 0, vtosAnno) * 100;

  respuesta.msg = "Cuadro Calculado";
  respuesta.porcTae = porcTae;
  respuesta.cuadro = cuadroAmort;


  return respuesta;
}

function CuadroAmortizacion(Prestamo)
	{
		var Ptmo = new Object();
			Ptmo.fechaInicio;
			Ptmo.importeInicial;
			Ptmo.diaPago;
			Ptmo.numCuotas;
			Ptmo.perAmo;
			Ptmo.sisAmr;
			Ptmo.indCapAjuste;
			Ptmo.claInt;
			Ptmo.aplicaLCI;
			Ptmo.intInicial;
			Ptmo.mesesIntini;
			Ptmo.intMixto;
			Ptmo.indCare;
			Ptmo.mesesCare;
			Ptmo.perCare;
			Ptmo.tipAnno;
			Ptmo.porcInc;
			Ptmo.cuoFinal;
			Ptmo.impCuota;
			Ptmo.indAjuste;
			Ptmo.capAjuste;
			Ptmo.fechaAjuste;
			Ptmo.indAmplia;
			Ptmo.impAmplia;
			Ptmo.fecAmplia;
			Ptmo.mesesPercare;
			Ptmo.mesesPeramo;
			Ptmo.impPdte;
			Ptmo.impTotalCarencia;
			Ptmo.Fechas;
			Ptmo.Cuadro;

		var Cuadro = [];
		var razon_prog = 1;

		Ptmo = Prestamo;

		Ptmo.impTotalCarencia = 0;

		if (!Ptmo.indAmplia)
		{
			Ptmo.impPdte = Ptmo.importeInicial;
		}
		else
		{
			Ptmo.impPdte = Ptmo.importeInicial + Ptmo.impAmplia;
		}

		switch (Ptmo.perAmo)
		{
			case "men":
				Ptmo.mesesPeramo = 1;

				break;

			case "tri":
				Ptmo.mesesPeramo = 3;

				break;

			case "sem":
				Ptmo.mesesPeramo = 6;

				break;

			case "anu":
				Ptmo.mesesPeramo = 12;

				break;
		}

		switch (Ptmo.perCare)
		{
			case "men":
				Ptmo.mesesPercare = 1;

				break;

			case "tri":
				Ptmo.mesesPercare = 3;

				break;

			case "sem":
				Ptmo.mesesPercare = 6;

				break;

			case "anu":
				Ptmo.mesesPercare = 12;

				break;

			case "ven":
			case "vca":
				Ptmo.mesesPercare = 0;

				break;
		}

		Ptmo.Fechas = new calcularFechas(Ptmo);

		if (Ptmo.indCare || Ptmo.Fechas.prdAjt || Ptmo.indAmplia)
			{
				Cuadro = calculoAjusteCarencia(Ptmo);

				if (Ptmo.perCare == "vca" || (Ptmo.Fechas.prdAjt && Ptmo.indCapAjuste))
				{
					Cuadro[0].imp_capital = Cuadro[0].imp_interes * -1;
					Ptmo.impPdte += Ptmo.impTotalCarencia;
				}
			}

			vtos_anno = calcular_vtosanno(Ptmo.perAmo);

			switch (Ptmo.sisAmr)
			{
				case "cFRA":
				case "cGCP":

					if (Ptmo.sisAmr == "cGCP")
					{
						razon_prog = 1 + (Ptmo.porcInc / 100);
					}

					calculo_cuadro(Ptmo, Cuadro, razon_prog, 0, vtos_anno);

					break;

				case "cFIN":

					var desg_final;

					Ptmo.numCuotas--;

					calculo_cuadro(Ptmo, Cuadro, razon_prog, 0, vtos_anno);

					if (Ptmo.claInt == "M")
					{
						desg_final = new conceptos_final(Ptmo.cuoFinal, Ptmo.intMixto, 360 / vtos_anno, Cuadro[Cuadro.length - 1].fec_cuota, Ptmo.mesesPeramo);
					}
					else
					{
						desg_final = new conceptos_final(Ptmo.cuoFinal, Ptmo.intInicial, 360 / vtos_anno, Cuadro[Cuadro.length - 1].fec_cuota, Ptmo.mesesPeramo);
					}

					Cuadro.push(desg_final);

					break;

				case "cBDD":

					Ptmo.numCuotas = 500;

					calculo_cuadro(Ptmo, Cuadro, razon_prog, imp_blind, vtos_anno);

					break;

				case "cCCP":

					var capital;

					capital = Math.round(imp_ptmo / num_cuotas * 100 ) / 100;

					calculo_cuadro(Ptmo, Cuadro, razon_prog, capital, vtos_anno);

					break;

		}

		Ptmo.Cuadro = Cuadro;

		return Ptmo;

	}

function calcularFechas(Ptmo)
	{
		var fechaTrabajo;
		var fechaTrabajo_2;
		var fechaFormString;
		var fechaTrabajoString;
		var fechaTrabajo_2String;
		var that = this;

		fechaTrabajo = sumarPlazoFecha(Ptmo.fechaInicio, 0, Ptmo.diaPago, true);

		fechaTrabajo_2 = sumarDiaFecha(Ptmo.fechaInicio, -1);

		fechaFormString = Ptmo.fechaInicio[0] + Ptmo.fechaInicio[1] + Ptmo.fechaInicio[2];
		fechaTrabajoString = fechaTrabajo[0] + fechaTrabajo[1] + fechaTrabajo[2];
		fechaTrabajo_2String = fechaTrabajo_2[0] + fechaTrabajo_2[1] + fechaTrabajo_2[2];

		if (fechaTrabajoString == fechaFormString || fechaTrabajoString == fechaTrabajo_2String)
		{
			that.prdAjt = false;

			that.fechaPriLiq = Ptmo.fechaInicio;
		}
		else
		{
			that.prdAjt = true;

			if (Ptmo.indAjuste && Ptmo.fechaAjuste != "")
			{
				that.fechaPriLiq = Ptmo.fechaAjuste;
			}
			else
			{
				if (fechaTrabajoString > fechaFormString)
				{
					that.fechaPriLiq = fechaTrabajo;
				}
				else
				{
					that.fechaPriLiq = sumarPlazoFecha(fechaTrabajo, 1, Ptmo.diaPago, true);
				}
			}
		}

		if (Ptmo.indCare)
		{
			if (Ptmo.mesesPercare == 0)
			{
				that.fechaPriCar = sumarPlazoFecha(that.fechaPriLiq, Ptmo.mesesCare, Ptmo.diaPago, true);
			}
			else
			{
				that.fechaPriCar = sumarPlazoFecha(that.fechaPriLiq, Ptmo.mesesPercare, Ptmo.diaPago, true);
			}

			that.fechaFinCar = sumarPlazoFecha(that.fechaPriLiq, Ptmo.mesesCare, Ptmo.diaPago, true);
		}
		else
		{
			that.fechaPriCar = that.fechaPriLiq;
			that.fechaFinCar = that.fechaPriCar;
		}

		that.fechaPriAmo = sumarPlazoFecha(that.fechaFinCar, Ptmo.mesesPeramo, Ptmo.diaPago, true);

		if (Ptmo.claInt == "M")
		{
			that.fechaPriRev = sumarPlazoFecha(that.fechaPriLiq, Ptmo.mesesIntini, Ptmo.diaPago, true);
		}
		else
		{
			that.fechaPriRev = that.fechaPriLiq;
		}

		return;
	}

function sumarPlazoFecha(fecha, plazo, diajuste, ajuste)
	{
		var fechaSalida = [];
		var fechaTrabajo;
		var anno;
		var mes;
		var dia;

		anno = Number(fecha[0]) + Math.trunc(plazo / 12);

		mes = Number(fecha[1]) + plazo % 12;

		if (mes > 12)
		{
			++anno;

			mes -= 12;
		}
		else
		{
			if (mes < 1)
			{
				mes += 12;
			}
		}

		if (ajuste)
		{
			if (diajuste == "31")
			{
				dia = 0;
				++mes;
			}
			else
			{
				dia = diajuste;
			}
		}
		else
		{
			dia = fecha[2]
		}

		if ((dia > 28 && mes == 2) || (dia == 31 && (mes == 4 || mes == 6 || mes == 9 || mes == 11)))
		{
			dia = 0;
			++mes;
		}

		fechaTrabajo = new Date(anno, mes - 1, dia);

		fechaSalida[0] = fechaTrabajo.getFullYear();
		fechaSalida[1] = Number(fechaTrabajo.getMonth()) + 1;

		if (fechaSalida[1] < 10)
		{
			fechaSalida[1] = "0" + fechaSalida[1];
		}

		fechaSalida[2] = fechaTrabajo.getDate();

		if (fechaSalida[2] < 10)
		{
			fechaSalida[2] = "0" + fechaSalida[2];
		}

		fechaSalida[0] = fechaSalida[0].toString();
		fechaSalida[1] = fechaSalida[1].toString();
		fechaSalida[2] = fechaSalida[2].toString();

		return fechaSalida;
	}

function sumarDiaFecha(fecha, dias)
	{
		var fechaJuliana;

		fechaJuliana = gregorianaAjuliana(fecha) + dias;

		return julianaAgregoriana(fechaJuliana);
	}

function diferenciaFechas(fecha1, fecha2, anno, inc1, inc2)
	{
		var juliana1;
		var juliana2;

		if (anno == "C")
		{
			juliana1 = gregorianaAjulianaC(fecha1);
			juliana2 = gregorianaAjulianaC(fecha2);
		}
		else
		{
			juliana1 = gregorianaAjuliana(fecha1);
			juliana2 = gregorianaAjuliana(fecha2);
		}

		return juliana2 - juliana1;
	}

function gregorianaAjuliana(fecha)
	{
		var dia;
		var mes;
		var anno;
		var jd1;
		var a2;
		var jd2;
		var jd3;
		var jd4;
		var diasJuliana;

		dia = Number(fecha[2]);
		mes = Number(fecha[1]);

		if (mes > 2)
		{
			mes -= 3;

			anno = Number(fecha[0]);
		}
		else
		{
			mes += 9;

			anno = Number(fecha[0]) - 1;
		}

		jd1 = Math.trunc(anno / 4000) * 1460969;
		a2 = anno % 4000;
		jd2 = Math.trunc((Math.trunc(a2 / 100) * 146097) / 4);
		jd3 = Math.trunc(((a2 % 100) * 1461) / 4);
		jd4 = Math.trunc(((153 * mes) + 2) / 5);

		diasJuliana = jd1 + jd2 + jd3 + jd4 + dia + 1721119 - 0.5;

		return diasJuliana;
	}

function julianaAgregoriana(fechaJul)
	{
		var f1;
		var f2;
		var f3;
		var m;
		var n;
		var p;
		var q;
		var r;
		var s;
		var fecha = [];

		f1 = fechaJul + 0.5;
		f2 = Math.trunc((f1 - 1867216.25) / 36524.25);
		f3 = Math.trunc(f2 / 4);
		m = f1 + f2 - f3 + 1;
		n = m + 1524;
		p = Math.trunc((n - 122.1) / 365.25);
		q = Math.trunc(p * 365.25);
		r = Math.trunc(((n - q) / 30.6001));
		s = Math.trunc(r * 30.6001);

		fecha[2] = n - q - s;

		if (r <= 13)
		{
			fecha[1] = r - 1;
		}
		else
		{
			fecha[1] = 	r - 13;
		}

		if (fecha[1] < 3)
		{
			fecha[0] = p - 4715;
		}
		else
		{
			fecha[0] = p - 4716;
		}

		if (fecha[2] < 10)
		{
			fecha[2] = "0" + fecha[2];
		}

		if (fecha[1] < 10)
		{
			fecha[1] = "0" + fecha[1];
		}

		fecha[0] = fecha[0].toString();
		fecha[1] = fecha[1].toString();
		fecha[2] = fecha[2].toString();

		return fecha;
	}

function calculoAjusteCarencia(Ptmo)
	{
		var fechaInicio;
		var fechaCalculo;
		var fechaTrabajo;
		var fechaPrxLiq;
		var fechaUltLiq;
		var carencia;
		var ajusteAmpl;
		var cuadro = [];

		carencia = Ptmo.indCare;

		if (Ptmo.Fechas.prdAjt || Ptmo.mesesPercare == 0 || Ptmo.indAmplia)
		{
			if (Ptmo.indAmplia || Ptmo.aplicaLCI)
			{
				fechaInicio = Ptmo.fechaInicio;
			}
			else
			{
				fechaInicio = sumarDiaFecha(Ptmo.fechaInicio, -1);
			}

			if (Ptmo.mesesPercare == 0)
			{
				cuadro[0] = new liquidaInteres(Ptmo.importeInicial, fechaInicio, Ptmo.Fechas.fechaFinCar, Ptmo.claInt, Ptmo.Fechas.fechaPriRev, Ptmo.intInicial, Ptmo.intMixto, Ptmo.tipAnno);

				carencia = false;
			}
			else
			{
				if (carencia || Ptmo.Fechas.prdAjt)
				{
					cuadro[0] = new liquidaInteres(Ptmo.importeInicial, fechaInicio, Ptmo.Fechas.fechaPriLiq, Ptmo.claInt, Ptmo.Fechas.fechaPriRev, Ptmo.intInicial, Ptmo.intMixto, "nn");
				}
				else
				{
					cuadro[0] = new liquidaInteres(Ptmo.importeInicial, fechaInicio, Ptmo.Fechas.fechaPriAmo, Ptmo.claInt, Ptmo.Fechas.fechaPriRev, Ptmo.intInicial, Ptmo.intMixto, "nn");
				}
			}

			if (Ptmo.indAmplia)
			{
				if (carencia)
				{
					if (Ptmo.aplicaLCI)
					{
						ajusteAmpl = new liquidaInteres(Ptmo.impAmplia, Ptmo.fecAmplia, Ptmo.Fechas.fechaPriLiq, Ptmo.claInt, Ptmo.Fechas.fechaPriRev, Ptmo.intInicial, Ptmo.intMixto, "nn");
					}
					else
					{
						ajusteAmpl = new liquidaInteres(Ptmo.impAmplia, sumarDiaFecha(Ptmo.fecAmplia, -1), Ptmo.Fechas.fechaPriLiq, Ptmo.claInt, Ptmo.Fechas.fechaPriRev, Ptmo.intInicial, Ptmo.intMixto, "nn");
					}
				}
				else
				{
					if (Ptmo.aplicaLCI)
					{
						ajusteAmpl = new liquidaInteres(Ptmo.impAmplia, Ptmo.fecAmplia, Ptmo.Fechas.fechaPriAmo, Ptmo.claInt, Ptmo.Fechas.fechaPriRev, Ptmo.intInicial, Ptmo.intMixto, "nn");
					}
					else
					{
						ajusteAmpl = new liquidaInteres(Ptmo.impAmplia, sumarDiaFecha(Ptmo.fecAmplia, -1), Ptmo.Fechas.fechaPriAmo, Ptmo.claInt, Ptmo.Fechas.fechaPriRev, Ptmo.intInicial, Ptmo.intMixto, "nn");
					}
				}

				cuadro[0].imp_interes += ajusteAmpl.imp_interes;

				Ptmo.importeInicial += Ptmo.impAmplia;
			}

			Ptmo.impTotalCarencia += cuadro[0].imp_interes;
		}

		if (carencia)
		{
			if (!Ptmo.Fechas.prdAjt && !Ptmo.aplicaLCI)
			{
				fechaUltLiq = sumarDiaFecha(Ptmo.Fechas.fechaPriLiq, -1);
			}
			else
			{
				fechaUltLiq = Ptmo.Fechas.fechaPriLiq;
			}

			fechaPrxLiq = Ptmo.Fechas.fechaPriCar;
			fechaCalculo = Ptmo.Fechas.fechaPriCar[0] + Ptmo.Fechas.fechaPriCar[1] + Ptmo.Fechas.fechaPriCar[2];
			fechaTrabajo = Ptmo.Fechas.fechaFinCar[0] + Ptmo.Fechas.fechaFinCar[1] + Ptmo.Fechas.fechaFinCar[2];

			for (var i = cuadro.length; fechaCalculo <= fechaTrabajo && i<= 1001; i++)
			{
				cuadro[i] = new liquidaInteres(Ptmo.importeInicial, fechaUltLiq, fechaPrxLiq, Ptmo.claInt, Ptmo.Fechas.fechaPriRev, Ptmo.intInicial, Ptmo.intMixto, Ptmo.tipAnno);

				Ptmo.impTotalCarencia += cuadro[i].imp_interes;

				fechaUltLiq = fechaPrxLiq;
				fechaPrxLiq = sumarPlazoFecha(fechaUltLiq, Ptmo.mesesPercare, Ptmo.diaPago, true);
				fechaCalculo = fechaPrxLiq[0] + fechaPrxLiq[1] + fechaPrxLiq[2];
			}
		}

		return cuadro;
	}

function liquidaInteres(importe, fechaIni, fechaFin, mixto, fechaCambio, interes1, interes2, tipoAnno)
	{
		var that = this;
		var dias;
		var dias1;
		var dias2;
		var fechaTrabajo1;
		var fechaTrabajo2;
		var fechaTrabajo3;
		var numerador;
		var divisor;
		var importeInteres1;
		var importeInteres2;

		switch (tipoAnno)
		{
			case "cc":

				numerador = "C";
				divisor = 36000;

				break;

			case "cn":

				numerador = "C";
				divisor = 36500;

				break;

			case "nc":

				numerador = "N";
				divisor = 36000;

				break;

			case "nn":

				numerador = "N";
				divisor = 36500;

				break;
		}

		that.fec_cuota = fechaFin;
		that.imp_pdte = importe;
		that.imp_capital = 0;

		fechaTrabajo1 = fechaIni[0] + fechaIni[1] + fechaIni[2];
		fechaTrabajo2 = fechaFin[0] + fechaFin[1] + fechaFin[2];
		fechaTrabajo3 = fechaCambio[0] + fechaCambio[1] + fechaCambio[2];

		if (mixto == "M" && fechaTrabajo3 > fechaTrabajo1 && fechaTrabajo3 < fechaTrabajo2)
		{
			dias1 = diferenciaFechas(fechaIni, fechaCambio, numerador, false, true);

			importeInteres1 = importe * interes1 * dias1 / divisor;
			importeInteres1 = Math.round(importeInteres1 * 100) / 100;

			dias2 = diferenciaFechas(fechaCambio, fechaFin, numerador, false, true);

			importeInteres2 = importe * interes2 * dias2 / divisor;
			importeInteres2 = Math.round(importeInteres2 * 100) / 100;

			that.imp_interes = importeInteres1 + importeInteres2;

			dias = dias1 + dias2;
		}
		else
		{
			dias = diferenciaFechas(fechaIni, fechaFin, numerador, false, true);

			if (mixto == "M" && fechaTrabajo2 > fechaTrabajo3)
			{
				that.imp_interes = importe * interes2 * dias / divisor;
			}
			else
			{
				that.imp_interes = importe * interes1 * dias / divisor;
			}

			that.imp_interes = Math.round(that.imp_interes * 100) / 100;
		}

		that.numdias = dias;

		return;
	}

function calcular_vtosanno(perio)
	{
		var numVtos;

		switch (perio)
		{
			case "men":

				numVtos = 12;

				break;

			case "tri":

				numVtos = 4;

				break;

			case "sem":

				numVtos = 2;

				break;

			case "anu":

				numVtos = 1;

				break;

		}

		return numVtos;
	}

function Cuota_perio(cuo_base, base_calc, tipo_int, num_dias, cod_num, cod_div, fecha_amr)
	{
		var that = this;

		if (cod_div === "comercial")
		{
			var divisor = 360;
		}
		else
		{
			var divisor = 365;
		}

		that.fec_cuota = fecha_amr;
		that.imp_pdte = base_calc;
		that.imp_interes = base_calc * tipo_int * num_dias / (divisor * 100);
		that.imp_interes = Math.round(that.imp_interes * 100 ) / 100;
		that.imp_capital = Math.round((cuo_base - that.imp_interes) * 100) / 100;

		if (that.imp_capital > base_calc)
		{
			that.imp_capital = base_calc;
		}

		that.numdias = num_dias;

		return;
	}

function Cuota_prop(cap_prop, base_calc, tipo_int, num_dias, cod_num, cod_div, fecha_amr)
	{
		var that = this;

		if (cod_div === "comercial")
		{
			var divisor = 360;
		}
		else
		{
			var divisor = 365;
		}

		that.fec_cuota = fecha_amr;
		that.imp_pdte = base_calc;
		that.imp_interes = base_calc * tipo_int * num_dias / (divisor * 100);
		that.imp_interes = Math.round(that.imp_interes * 100 ) / 100;
		that.imp_capital = cap_prop;

		if (that.imp_capital > base_calc)
		{
			that.imp_capital = base_calc;
		}

		that.numdias = num_dias;

		return;
	}

function calc_cuo_fra(base_calc, num_plazo, tip_perio, porc_int, raz_prog)
	{
		var tip_int = porc_int / (tip_perio * 100);
    var exp1 = Math.pow((1 + tip_int), tip_perio);
    var parte1 = base_calc * tip_int * (exp1 - raz_prog);
    var parte2 = 1 / exp1;
    var exp2 = Math.pow(parte2, (num_plazo / tip_perio));
    var parte3 = 1 - exp2;
    var parte4 = parte3 * (exp1 - 1);
    var imp_cuota = parte1 / parte4;

		return imp_cuota;
	}

function calculo_cuadro(Ptmo, Cuadro, r_prog, importe_e, vtos_e)
	{
		var imp_cuota = 0;
		var num_meses = 0;
		var val_interes;
		var fecha_cuota;
		var plazoAmr;
		var fechaRev;
		var inicio;
		var fin;
		var imp_sinfinal;
		var desg_final;
		var cuota1;
		var cuota2;
		var mixto;

		if (Ptmo.claInt == "M")
		{
			fechaRev = Ptmo.Fechas.fechaPriRev[0] + Ptmo.Fechas.fechaPriRev[1] + Ptmo.Fechas.fechaPriRev[2];

			mixto = true;
		}

		if (Ptmo.indAmplia && !carencia)
		{
			fecha_cuota = sumarPlazoFecha(Ptmo.Fechas.fechaPriAmo, Ptmo.mesesPeramo, Ptmo.diaPago, true);
		}
		else
		{
			fecha_cuota = Ptmo.Fechas.fechaPriAmo;
		}

		plazoAmr = 12 / vtos_e;

		if ((Ptmo.indAjuste || (Ptmo.indAmplia && !Ptmo.indCare)) && Cuadro.length == 1)
		{
			Cuadro[0].imp_capital = Ptmo.capAjuste;

			Ptmo.impPdte = Math.round((Ptmo.impPdte - Cuadro[0].imp_capital) * 100) / 100;
			Ptmo.numCuotas--;
		}

		if (Ptmo.sisAmr == "cBDD")
		{
			imp_cuota = importe_e;
		}
		else
		{
			if (Ptmo.sisAmr !== "cCCP")
			{
				if (Ptmo.sisAmr == "cFIN")
				{
					desg_final = new conceptos_final(Ptmo.cuoFinal, Ptmo.intInicial, 360 / vtos_e, fecha_cuota, Ptmo.mesesPeramo);

					imp_sinfinal = Ptmo.impPdte - desg_final.imp_capital;

					imp_cuota = calc_cuo_fra(imp_sinfinal, Ptmo.numCuotas, vtos_e, Ptmo.intInicial, r_prog);

					cuota1 = new Cuota_perio(imp_cuota, imp_sinfinal, Ptmo.intInicial , 360 / vtos_e, 360, "comercial", fecha_cuota);
					cuota2 = new Cuota_perio(imp_cuota, Ptmo.impPdte, Ptmo.intInicial , 360 / vtos_e, 360, "comercial", fecha_cuota);

					imp_cuota = cuota1.imp_capital + cuota2.imp_interes;
				}
				else
				{
          imp_cuota = calc_cuo_fra(Ptmo.impPdte, Ptmo.numCuotas, vtos_e, Ptmo.intInicial, r_prog);
				}
			}
		}

		inicio = Cuadro.length;
		total = inicio + Ptmo.numCuotas;
		val_interes = Ptmo.intInicial;

		for (var i = inicio; i < total && Ptmo.impPdte > 0 ; i++)
		{
			if (Ptmo.sisAmr == "cGCP")
			{
				num_meses++;

				if (num_meses > vtos_e)
				{
					imp_cuota = Math.round(imp_cuota * r_prog * 100) / 100;

					num_meses = 1;
				}
			}

			fechaTrabajo = fecha_cuota[0] + fecha_cuota[1] + fecha_cuota[2];

			if (mixto && (fechaTrabajo > fechaRev))
			{
				val_interes = Ptmo.intMixto;

				if (Ptmo.sisAmr != "cBDD" && Ptmo.sisAmr != "cCCP")
				{
					if (Ptmo.sisAmr == "cFIN")
					{
						desg_final = new conceptos_final(Ptmo.cuoFinal, val_interes, 360 / vtos_e, fecha_cuota, Ptmo.mesesPeramo);

						imp_sinfinal = Ptmo.impPdte - desg_final.imp_capital;

						imp_cuota = calc_cuo_fra(imp_sinfinal, Ptmo.numCuotas - (i - inicio), vtos_e, val_interes, r_prog);

						cuota1 = new Cuota_perio(imp_cuota, imp_sinfinal, val_interes , 360 / vtos_e, 360, "comercial", fecha_cuota);
						cuota2 = new Cuota_perio(imp_cuota, Ptmo.impPdte, val_interes , 360 / vtos_e, 360, "comercial", fecha_cuota);

						imp_cuota = cuota1.imp_capital + cuota2.imp_interes;
					}
					else
					{
						imp_cuota = calc_cuo_fra(Ptmo.impPdte, Ptmo.numCuotas - (i - inicio), vtos_e, val_interes, r_prog);
					}

					if (Ptmo.sisAmr == "cGCP")
					{
						num_meses = 1;
					}
				}

				mixto = false;
			}

			if (Ptmo.sisAmr == "cCCP")
			{
				Cuadro[i] = new Cuota_prop(importe_e, Ptmo.impPdte, val_interes , 360 / vtos_e, 360, "comercial", fecha_cuota);
			}
			else
			{
				Cuadro[i] = new Cuota_perio(imp_cuota, Ptmo.impPdte, val_interes , 360 / vtos_e, 360, "comercial", fecha_cuota);
			}

			Ptmo.impPdte = Math.round((Ptmo.impPdte - Cuadro[i].imp_capital) * 100) / 100;

			fecha_cuota = sumarPlazoFecha(fecha_cuota, Ptmo.mesesPeramo, Ptmo.diaPago, true);
		}

		if (Ptmo.sisAmr == "cFIN")
		{
			Ptmo.impPdte -= desg_final.imp_capital;
		}

		Cuadro[i - 1].imp_capital = Cuadro[i - 1].imp_capital + Ptmo.impPdte;

		return;
	}

function conceptos_final(importe, porcentaje, dias, fecha, meses)
	{
		var that = this;

		that.fec_cuota = sumarPlazoFecha(fecha, meses, dia_pago, true);
		that.imp_pdte = Math.round((importe / (1 + (porcentaje * dias / 36000)) * 100 )) / 100;
		that.imp_capital = Math.round((importe / (1 + (porcentaje * dias / 36000)) * 100 )) / 100;
		that.imp_interes = Math.round((importe - that.imp_capital) * 100) / 100;
		that.numdias = dias;

		return;
	}

function calcularTAE(importe_Finan, cuadro_amort, fecha_formaliza, dia_pago, imp_apertura , imp_bonifica, imp_gasform, imp_seguro, imp_correo, imp_gastsegu, imp_gastmante, imp_tarjeta, vtos)
	{
		var maxIter = 1000;
		var valc;
		var tir_a = -1;
		var tir_p = 0;
		var a = 0;
		var b = 2 / 360;
		var c;
		var d;
		var total;
		var fechaGastos;
		var fechaTrabajo1;
		var fechaTrabajo2;

		importeFinan = importe_Finan - imp_apertura + imp_bonifica - imp_gasform - imp_seguro - imp_gastmante - imp_gastsegu - imp_tarjeta;

		for (var n = 0; n < maxIter ; n++)
		{
			valc = 0;
			c = (a + b) / 2;
			d = 0;

			fechaGastos = sumarPlazoFecha(fecha_formaliza, 12, dia_pago, true);

			for (var m = 0; m < cuadro_amort.length; m++)
			{
				d += cuadro_amort[m].numdias;

				total = cuadro_amort[m].imp_capital + cuadro_amort[m].imp_interes + imp_correo;

				fechaTrabajo1 = fechaGastos[0] + fechaGastos[1] + fechaGastos[2];
				fechaTrabajo2 = cuadro_amort[m].fec_cuota[0] + cuadro_amort[m].fec_cuota[1] + cuadro_amort[m].fec_cuota[2]

				if (fechaTrabajo2 >= fechaTrabajo1 && m != (cuadro_amort.length - 1))
				{
					total = cuadro_amort[m].imp_capital + cuadro_amort[m].imp_interes + imp_correo + imp_gastmante + imp_gastsegu + imp_tarjeta;

					fechaGastos = sumarPlazoFecha(fechaGastos, 12, dia_pago, true);
				}

//				valc += total / (1 + c) ** d;
        	valc += total / Math.pow((1 + c), d);
			}

			if (valc < importeFinan)
			{
				b = c;
			}
			else
			{
				a = c;
			}

			if (Math.abs(valc - importeFinan) < 0.00001)
			{
				tir_p = c;
				tir_a = tir_p * 360;
				n = maxIter;
			}
		}

		if  (tir_a != -1)
		{
//			return ((1 + tir_p) ** 360) - 1;
      	return Math.pow((1 + tir_p), 360) - 1;
		}
		else
		{
			return 0;
		}
	}

module.exports.calculoCuadro = calculoCuadro;
