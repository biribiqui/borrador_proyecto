const requestJson = require("request-json");

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechuagm12ed/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getMovesById(req, res){

    console.log("GET /apitechu/moves/:id");

    var httpClient = requestJson.createClient(baseMLABUrl);

    console.log("Cliente creado");

    var id = Number.parseInt(req.params.idfolio);

    console.log("El folio a consultar es " + req.params.idfolio);

    var query = "q=" + JSON.stringify({"idfolio":id}) +'&s={"fevalor":-1, "fecalta":-1}';

    httpClient.get("moves?" + query + "&" + mlabAPIKey, function(err, resMlab, body){
      if (err){

        var response = {"msg": "Error obteniendo movimientos"};
      }else {
        if (body.length > 0) {

          var response = body;
        }
        else {

          var response = {"msg": "Cuenta sin movimientos"};
        }
      }

      res.send(response);
    }
  );
}

function createMoves(req, res){

  console.log("GET /apitechu/moves/alta");

  var httpClient = requestJson.createClient(baseMLABUrl);

  console.log("Cliente creado");

  var fecha = new Date();

  var newMove = {
     "idfolio":req.body.idfolio,
     "fecalta":fecha,
     "fecvalor":req.body.fecvalor,
     "tipmvto":req.body.tipmvto,
     "importe":req.body.importe,
     "ibanorigen":req.body.ibanorigen,
     "ibandestino":req.body.ibandestino,
     "notas":req.body.notas
   }

   httpClient.post("moves?" + mlabAPIKey, newMove,
    function(err, resMlab, body){
      if (err){

        var response = {"msg": "Error en alta de movimiento"};

        res.send(response);
      }else {
         console.log("Movimiento creado");

         res.send({"msg":"Movimiento Creado", "idfolio":req.body.idfolio, "fecalta": fecha});
      }
    }
  );
}

module.exports.getMovesById = getMovesById;
module.exports.createMoves = createMoves;
