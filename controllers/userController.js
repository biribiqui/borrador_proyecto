const crypt = require("../crypt");

const requestJson = require("request-json");
const jwt = require("jsonwebtoken");
const bodyParser = require("body-parser");

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechuagm12ed/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function createUser(req, res){

  console.log("POST /apitechu/users/alta");

  var query = 'q={"$and":[{"activo":true},' + JSON.stringify({"email":req.body.email}) + ']}';

  var httpClient = requestJson.createClient(baseMLABUrl);

  httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body){
      if (err){
         var response = {"msg": "Error en alta de usuario"};

         res.send(response);
       }
      else {
        if (body.length > 0){

          var response = {"msg": "Usuario ya existe"};

          res.send(response);
        }
        else {
         query = 'f={"id":1}&l=1&s={"id":-1}';

         httpClient.get("user?" + query + "&" + mlabAPIKey,
          function(err, resMlab, getidbody){
            if (err){
                var response = {"msg": "Error en alta de usuario"};

                res.send(response);
            }
            else {
              if (getidbody.length == 0){
                  var idUsuario = 1;
              }
              else {
                  var idUsuario = Number.parseInt(getidbody[0].id) + 1;
              }

              var newUser = {
                "id":idUsuario,
                "first_name":req.body.nombre,
                "last_name":req.body.apellido,
                "domicilio":req.body.domicilio,
                "provincia":req.body.provincia,
                "codposdtal":req.body.codpostal,
                "nif":req.body.nif,
                "telefono":req.body.telefono,
                "email":req.body.email,
                "password":crypt.hash(req.body.password),
                "activo":true,
                "fecalta": req.body.fecalta,
                "logged":true
              }

              httpClient.post("user?" + mlabAPIKey, newUser,
                function(err, resMlab, postbody){
                  if (err){

                    var response = {"msg": "Error en alta de usuario"};

                    res.send(response);
                  }
                  else {
                    console.log("Usuario creado");

                    var tokenData = {
                      "username":req.body.email
                    };

                    var tokenjwt = jwt.sign(tokenData, "Secret Password", {expiresIn: 60 * 60 * 2});

                    res.send({"msg":"Usuario Creado", "id":idUsuario, "token":tokenjwt});
                    }
                  }
                );
              }
            }
          );
        }
      }
    }
  );
}

function deleteUser(req, res){
  console.log("POST /apitechu/users/baja/:id ");

  console.log("El id del usuario a borrar " + req.params.id);

  var id = Number.parseInt(req.params.id);

  var query = 'q={"$and":[{"activo":true},' + JSON.stringify({"id": id}) + ']}';

  var httpClient = requestJson.createClient(baseMLABUrl);

  httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body){

      if (err)
      {
        var response = {"msg": "Error en baja de usuario"};

        res.send(response);
      }
      else
      {
        if (body.length > 0)
        {
          id = Number.parseInt(body[0].id);
          query = "q=" + JSON.stringify({"id": id});

          var putBody = '{"$unset":{"logged":""}, "$set":{"activo":false, "febaja":"' + req.body.febaja + '"}}';

          httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
            function(err, resMlab, bodyPut){
              if (err)
              {
                var response = {"msg": "Error en baja de usuario"};

                res.send(response);
              }
              else
              {
                var response = {"msg":"Baja correcta", "idUsuario" : body[0].id};

                res.send(response);
              }
            }
          );
        }
        else
        {
          var response = {"msg": "Usuario no encontrado"};
          res.status(404);

          res.send(response);
        }
      }
    }
  );
}

function updateUser(req, res){
  console.log("POST /apitechu/users/modifica/:id ");

  console.log("El id del usuario a modificar " + req.params.id);

  var id = Number.parseInt(req.params.id);

  var query = 'q={"$and":[{"activo":true},' + JSON.stringify({"id": id}) + ']}';

  var httpClient = requestJson.createClient(baseMLABUrl);

  httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body){

      if (err)
      {
        var response = {"msg": "Error en modificación de usuario"};

        res.send(response);
      }
      else
      {
        if (body.length > 0)
        {
          id = Number.parseInt(body[0].id);
          query = "q=" + JSON.stringify({"id": id});

          var putBody = '{"$set":{' +
            '"first_name":"' + req.body.nombre + '", ' +
            '"last_name":"' + req.body.apellido + '", ' +
            '"domicilio":"' + req.body.domicilio + '", ' +
            '"provincia":"' + req.body.provincia + '", ' +
            '"codposdtal":"' + req.body.codpostal + '", ' +
            '"nif":"' + req.body.nif + '", ' +
            '"telefono":"' + req.body.telefono + '"' +
            '}}';

          httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
            function(err, resMlab, bodyPut){
              if (err)
              {
                var response = {"msg": "Error en modificación de usuario"};

                res.send(response);
              }
              else
              {
                var response = {"msg":"Modificación correcta", "idUsuario" : body[0].id};

                res.send(response);
              }
            }
          );
        }
        else
        {
          var response = {"msg": "Usuario no encontrado"};
          res.status(404);

          res.send(response);
        }
      }
    }
  );
}

module.exports.createUser = createUser;
module.exports.deleteUser = deleteUser;
module.exports.updateUser = updateUser;
