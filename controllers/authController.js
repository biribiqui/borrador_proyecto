const crypt = require("../crypt");

const requestJson = require("request-json");
const jwt = require("jsonwebtoken");
const bodyParser = require("body-parser");

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechuagm12ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function logonUser(req, res){

  console.log("POST /apitechu/users/login");

  var query = 'q={"$and":[{"activo":true},' + JSON.stringify({"email":req.body.email}) + ']}';

  var httpClient = requestJson.createClient(baseMLABUrl);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
      if (err){
        console.log("Error: " + err)
        var response = {"msg": "Error obteniendo usuario"};

        res.send(response);
      }
      else {
        if (body.length > 0)
        {
          if (checkPassword(req.body.password, body[0].password) === true)
          {
            id = Number.parseInt(body[0].id);
            query = "q=" + JSON.stringify({"id": id});

            var putBody = '{"$set":{"logged":true}}';

            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(err, resMlab, bodyPut){
                if (err)
                {
                  var response = {"msg": "Error logando usuario"};

                  res.send(response);
                }
                else
                {
                  var tokenData = {
                    "username":req.body.email
                  };

                  var tokenjwt = jwt.sign(tokenData, "Secret Password", {expiresIn: 60 * 60 * 2});

                    var response = {
                    "msg":"Login correcto",
                    "idUsuario" : body[0].id,
                    "email": req.body.email,
                    "first_name": body[0].first_name,
                    "apellido": body[0].last_name,
                    "domicilio": body[0].domicilio,
                    "provincia": body[0].provincia,
                    "codpostal": body[0].codposdtal,
                    "nif": body[0].nif,
                    "telefono": body[0].telefono,
                    "token": tokenjwt
                  };

                  res.send(response);
                }
              }
            );
          }
          else
          {
            var response = {"msg": "Datos incorrectos"};

            res.send(response);
          }
        }
        else
        {
          var response = {"msg": "Datos incorrectos"};
          res.status(404);

          res.send(response);
        }
      }
    }
  );
}

function logoutUser(req, res){
  console.log("POST /apitechu/users/logout/:id " + req.params);

  console.log("El id del usuario a deslogar " + req.params.id);

  var id = Number.parseInt(req.params.id);
  var query = "q=" + JSON.stringify({"id": id});

  var httpClient = requestJson.createClient(baseMLABUrl);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){

      if (err)
      {
        var response = {"msg": "Error obteniendo usuarios"};

        res.send(response);
      }
      else
      {
        if (body.length > 0)
        {
          if (body[0].logged === true)
          {
            id = Number.parseInt(body[0].id);
            query = "q=" + JSON.stringify({"id": id});

            var putBody = '{"$unset":{"logged":""}}'

            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(err, resMlab, bodyPut){
                if (err)
                {
                  var response = {"msg": "Error deslogando usuario"};

                  res.send(response);
                }
                else
                {
                  var response = {"msg":"Logout correcto", "idUsuario" : body[0].id};

                  res.send(response);
                }
              }
            );
          }
          else
          {
            var response = {"msg": "Error usuario no logado"};

            res.send(response);
          }
        }
        else
        {
          var response = {"msg": "Usuario no encontrado"};
          res.status(404);

          res.send(response);
        }
      }
    }
  );
}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
 console.log("Checking password");

 return crypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}

module.exports.logonUser = logonUser;
module.exports.logoutUser = logoutUser;
